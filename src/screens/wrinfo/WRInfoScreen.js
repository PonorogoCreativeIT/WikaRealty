//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, ImageBackground, Dimensions, TouchableOpacity, ScrollView } from 'react-native';
import Pdf from 'react-native-pdf';
import { Navigation } from 'react-native-navigation';
import Colors from '../../utilities/Colors';
import BaseComponent from '../../components/BaseComponent';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
// create a component
class WRInfoScreen extends Component {
  static options(passProps) {
    return {
      topBar: {
        visible: true,
        title: {
          text: 'WR Info',
          color: 'white',
        },
        background: {
          color: Colors.PRIMARY_COLOR,
        },
        backButton: {
          color: '#fff',
          visible: true
        },
        rightButtons: [
          {
            id: 'home',
            icon: require('../../assets/images/ic_home.png')
          }
        ],
      },
      layout: {
        orientation: ['portrait'] // An array of supported orientations
      },
    };
  }

  constructor(props){
    super(props);
    Navigation.events().bindComponent(this);
  }

  navigationButtonPressed({ buttonId }) {
    if(buttonId === 'home'){
      Navigation.setStackRoot(this.props.componentId, {
        component: {
          name: 'HomeScreen',
        }
      });
    }
  }

  goToShe(){
    Navigation.push(this.props.componentId, {
      component: {
        name: 'ListDataScreen',
        passProps: {
          name: 'SHE',
          parentName: 'WR Info',
          params: [
            {
              name: 'nama_sub_modul_i',
              data: '4.1 SHE',
            },
            {
              name: 'des_sub_modul_i',
              data: 'Berikut adalah sub modul SHE'
            },
            {
              name: 'img_sub_modul_i',
              data: 'http://202.59.167.149/uploads/4/2018-05/wr_info_she.png'
            },
            {
              name: 'master_modul_id',
              data: 4
            },
            {
              name: 'sub_modul_i_id',
              data: 11
            },
          ]
        }
      }
    });
  }

  goToFiveR(){
    Navigation.push(this.props.componentId, {
      component: {
        name: 'ListDataScreen',
        passProps: {
          name: '5R',
          parentName: 'WR Info',
          params: [
            {
              name: 'nama_sub_modul_i',
              data: '4.2 5R',
            },
            {
              name: 'des_sub_modul_i',
              data: 'Berikut adalah sub module 5R'
            },
            {
              name: 'img_sub_modul_i',
              data: ''
            },
            {
              name: 'master_modul_id',
              data: 4
            },
            {
              name: 'sub_modul_i_id',
              data: 12
            },
          ]
        }
      }
    });
  }

  goToMutu(){
    Navigation.push(this.props.componentId, {
      component: {
        name: 'ListDataScreen',
        passProps: {
          name: 'MUTU',
          parentName: 'WR Info',
          params: [
            {
              name: 'nama_sub_modul_i',
              data: '4.3 Mutu',
            },
            {
              name: 'des_sub_modul_i',
              data: 'Berikut adalah sub module Mutu'
            },
            {
              name: 'img_sub_modul_i',
              data: 'http://202.59.167.149/uploads/4/2018-05/folder_icon.png'
            },
            {
              name: 'master_modul_id',
              data: 4
            },
            {
              name: 'sub_modul_i_id',
              data: 13
            },
          ]
        }
      }
    });
  }

  render() {
    return (
      <BaseComponent>
        <ImageBackground style={styles.container} source={require('../../assets/images/bg_menu.png')}>
          <View
            style={{backgroundColor: '#bfbfbf', padding: 5, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start'}}
          >
            <Image source={require('../../assets/images/kualitas_nav.png')} style={{width: 20, height: 20, resizeMode: 'contain'}}/>
            <Text style={{fontWeight: 'bold', fontSize: 13, marginLeft: 15, flex: 1, marginTop: 2}}>WR Info</Text>
          </View>
          <ScrollView style={{flex: 3, alignSelf: 'stretch', padding: 10}}>
            <View style={{flexDirection: 'row'}}>
              <TouchableOpacity
                onPress={() => this.goToShe()}
                style={{flex: 1, backgroundColor: '#8a9aaf', marginBottom: 5, marginRight: 5, alignItems: 'center', justifyContent: 'center', paddingTop: 25, paddingBottom: 25}}
              >
                <View style={{alignItems: 'center', justifyContent: 'center'}}>
                  <Image source={require('../../assets/images/wr_info_she.png')}/>
                  <Text style={{fontWeight: 'bold', color: '#fff'}}>SHE</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.goToFiveR()}
                style={{flex: 1, backgroundColor: '#8a9aaf', marginBottom: 5, marginLeft: 5, alignItems: 'center', justifyContent: 'center', paddingTop: 25, paddingBottom: 25}}
              >
                <View style={{alignItems: 'center', justifyContent: 'center'}}>
                  <Image source={require('../../assets/images/wr_info_5r.png')}/>
                  <Text style={{fontWeight: 'bold', color: '#fff'}}>5R</Text>
                </View>
              </TouchableOpacity>
            </View>
            <View style={{flexDirection: 'row'}}>
              <TouchableOpacity
                onPress={() => this.goToMutu()}
                style={{flex: 1, backgroundColor: '#8a9aaf', marginTop: 5, marginRight: 5, alignItems: 'center', justifyContent: 'center', paddingTop: 25, paddingBottom: 25}}
              >
                <View style={{alignItems: 'center', justifyContent: 'center'}}>
                  <Image source={require('../../assets/images/wr_info_mutu.png')}/>
                  <Text style={{fontWeight: 'bold', color: '#fff'}}>MUTU</Text>
                </View>
              </TouchableOpacity>
              <View style={{flex: 1}}/>
            </View>
          </ScrollView>
        </ImageBackground>
      </BaseComponent>
    );
  }
}

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});

//make this component available to the app
export default WRInfoScreen;
