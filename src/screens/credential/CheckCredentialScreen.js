//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, AsyncStorage } from 'react-native';
import LoadingScreen from '../../components/LoadingScreen';
import { Navigation } from 'react-native-navigation';
import Helpers from '../../utilities/Helpers';

// create a component
class CheckCredentialScreen extends Component {
  static options(passProps) {
    return {
      topBar: {
        visible: false,
      }
    };
  }

  constructor(props){
    super(props);
    Navigation.events().bindComponent(this);
    this.state = {
      isLoading: false,
    };
  }

  componentDidAppear() {
    this.checkNotifications();
    setTimeout(() => {
      this.checkLogin();
    }, 200);
  }

  async checkNotifications(){
    try{
      const notifications = await AsyncStorage.getItem('notifications');
      if(JSON.parse(notifications).length > 0){

      }
    }catch(error){
      await AsyncStorage.setItem('notifications', JSON.stringify([]));
    }
    
    // const tempNotifications = [{
    //   nama_qshe_file: 'test',
    //   des_qshe_file: 'test description',
    //   url_qshe_file: 'http://202.59.167.149/uploads/9/2018-05/kebijakan_perusahaan.pdf',
    // }];
    // await AsyncStorage.setItem('notifications', JSON.stringify(tempNotifications));
  }

  async checkLogin(){
    try{
      const data = await AsyncStorage.getItem('user');
      if(data !== null){
        Navigation.setStackRoot(this.props.componentId, {
          component: {
            name: 'HomeScreen',
          }
        });
      }else{
        Navigation.setStackRoot(this.props.componentId, {
          component: {
            name: 'LoginScreen',
          }
        });
      }
    }catch(error){
      Helpers.message(error);
    }
  }

  componentDidDisappear() {

  }

  render() {
    return (
      <View style={styles.container}>
        <LoadingScreen text={'Tunggu sebentar ...'}/>
      </View>
    );
  }
}

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
});

//make this component available to the app
export default CheckCredentialScreen;
