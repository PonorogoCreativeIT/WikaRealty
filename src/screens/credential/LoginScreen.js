//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, ImageBackground, ScrollView, Image, TextInput, TouchableOpacity, Dimensions, AsyncStorage } from 'react-native';
import { Navigation } from 'react-native-navigation';
import { TextInputLayout } from 'rn-textinputlayout';
import { Button } from 'react-native-elements'
import Modal from 'react-native-modalbox';
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
import Colors from '../../utilities/Colors';
import ForgotPasswordScreen from '../../components/ForgotPasswordScreen';
import Helpers from '../../utilities/Helpers';
import WikaAPI from '../../WikaAPI';

const widthScreen = Dimensions.get('window').width;
// create a component
class LoginScreen extends Component {
  static options(passProps) {
    return {
      topBar: {
        visible: false,
      },
      layout: {
        orientation: ['portrait'] // An array of supported orientations
      },
    };
  }

  constructor(props){
    super(props);
    Navigation.events().bindComponent(this);

    this.state = {
      statusBarHeight: 20,
      isForgotPasswordOpen: false,
      isLoading: false,
      nik: '',
      password_user: '',
    };
  }

  componentDidAppear(){
    this.loadConstant();
  }

  async loadConstant(){
    const constants = await Navigation.constants();
    const statusBarHeight = constants.statusBarHeight;

    this.setState({statusBarHeight});
  }

  async login(){
    await this.setState({isLoading: true});
    const formData = new FormData();
    formData.append('nik', this.state.nik);
    formData.append('password_user', this.state.password_user);
    const response = await WikaAPI.login(formData);
    await this.setState({isLoading: false});
    if(response.data.api_status === 1){
      if(response.data.status_user === 1){
        await AsyncStorage.setItem('user', JSON.stringify(response.data));
        Navigation.setStackRoot(this.props.componentId, {
          component: {
            name: 'HomeScreen',
            passProps: {
              isShowModalWelcome: true,
            }
          }
        });
      }else if(response.data.status_user === 2){
        Helpers.message('Whoopss!', 'This User is Not Active! Please Contact Administrator');
      }else{
        Helpers.message('Whoopss!', 'This User is Blocked! Please Contact Administrator');
      }
    }else{
      Helpers.message('Whoopss!', response.data.api_message);
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <ImageBackground style={[styles.container, {marginTop: this.state.statusBarHeight, flex: 1, alignSelf: 'stretch'}]} source={require('../../assets/images/bg_wika.png')}>
          <ScrollView style={{flex: 1, alignSelf: 'stretch'}}>
            <View style={{alignItems: 'center', justifyContent: 'center'}}>
              <Image source={require('../../assets/images/logo.png')} style={{marginTop: 25, width: 200, height: 100, resizeMode: "contain"}}/>
              <View style={{backgroundColor: '#fff', alignSelf: 'stretch', paddingTop: 15, paddingBottom: 15, paddingLeft: 25, paddingRight: 25, marginLeft: 25, marginRight: 25, marginTop: 10, borderRadius: 5}}>
                <TextInputLayout
                  focusColor={Colors.PRIMARY_COLOR}
                >
                  <TextInput
                    autoFocus
                    style={{height: 40, fontSize: 14}}
                    placeholder={'NIP'}
                    onChangeText={(nik) => this.setState({nik})}
                  />
                </TextInputLayout>
                <TextInputLayout
                  focusColor={Colors.PRIMARY_COLOR}
                  style={{marginTop: 5}}
                >   
                  <TextInput
                    style={{height: 40, fontSize: 14}}
                    placeholder={'Password'}
                    secureTextEntry
                    onChangeText={(password_user) => this.setState({password_user})}
                  />
                </TextInputLayout>
                <Button
                  onPress={() => this.login()}
                  borderRadius={5}
                  containerViewStyle={{marginTop: 30, marginLeft: 0, marginRight: 0}}
                  title='Login'
                  textStyle={{color: '#fff'}}
                  backgroundColor={Colors.PRIMARY_COLOR}
                />
                <View style={{marginTop: 10, alignItems: 'flex-end', justifyContent: 'flex-end',}}>
                  <TouchableOpacity onPress={() => this.setState({isForgotPasswordOpen: true})}>
                    <Text style={{color: Colors.PRIMARY_COLOR, padding: 10}}>Forgot Password?</Text>
                  </TouchableOpacity>
                </View>
              </View>
              <Image source={require('../../assets/images/logo_footer.png')} style={{ width: 300, height: 150, resizeMode: "contain"}}/>
              <Text>Coptyright © QSSHE</Text>
            </View>
          </ScrollView>
        </ImageBackground>
        <Modal
          isOpen={this.state.isForgotPasswordOpen}
          onClosed={() => this.setState({isForgotPasswordOpen: false})} 
          style={{justifyContent: 'center', alignItems: 'center', width: widthScreen - 50, height: null, paddingLeft: 0, paddingRight: 0}} position={"center"}
        >
          <View style={{alignSelf: 'stretch', height: null}}>
            <ImageBackground source={require('../../assets/images/head_profile.png')} style={{alignSelf: 'stretch', height: 125, resizeMode: 'cover', alignItems: 'center', justifyContent: 'center'}}>
              <Text style={{color: '#fff', fontSize: 25}}>LUPA PASSWORD?</Text>
            </ImageBackground>
            <View style={{padding: 22}}>
              <Text>Silahkan Hubungi Admin, Untuk Reset Password Anda.</Text>
              <Text style={{marginTop: 15}}>admin@wikarealty.co.id</Text>
              <View style={{marginTop: 20, alignItems: 'flex-end', justifyContent: 'flex-end',}}>
                <TouchableOpacity onPress={() => this.setState({isForgotPasswordOpen: false})}>
                  <Text style={{color: '#3498db', fontWeight: 'bold'}}>YA, SAYA HUBUNGI</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
        <OrientationLoadingOverlay
          visible={this.state.isLoading}
          color="white"
          indicatorSize="large"
          messageFontSize={24}
          message="Please wait..."
          />
      </View>
    );
  }
}

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.PRIMARY_COLOR,
  },
});

//make this component available to the app
export default LoginScreen;
