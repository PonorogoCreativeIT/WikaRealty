//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, AsyncStorage, TextInput, ScrollView, FlatList } from 'react-native';
import { Navigation } from 'react-native-navigation';
import { Avatar, Button } from 'react-native-elements';
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
import { TextInputLayout } from 'rn-textinputlayout';
import Colors from '../../utilities/Colors';
import Helpers from '../../utilities/Helpers';
import WikaAPI from '../../WikaAPI';
import BaseComponent from '../../components/BaseComponent';

// create a component
class SearchScreen extends Component {
  static options(passProps) {
    return {
      topBar: {
        visible: true,
        title: {
          text: 'Search',
          color: 'white',
        },
        background: {
          color: Colors.PRIMARY_COLOR,
        },
        backButton: {
          title: 'Back',
          color: '#fff',
          visible: true,
          showTitle: false,
        },
      },
      layout: {
        orientation: ['portrait'] // An array of supported orientations
      },
    };
  }

  constructor(props){
    super(props);
    Navigation.events().bindComponent(this);
    this.state = {
      isLoading: false,
      datas: [],
      keyword: '',
    };
  }

  componentDidMount(){
    this.loadDataPDF();
  }

  async loadDataPDF(){
    this.setState({
      datas: [],
      // isLoading: true
    });
    try{
      const response = await WikaAPI.search_file_pdf();
      this.setState({
        // isLoading: false,
        datas: response.data.data,
      })
    }catch(error){
      this.setState({
        // isLoading: false,
      })
    }
  }

  getData(){
    let result = [];

    for(let data of this.state.datas){
      if(
        data.nama_qshe_file.toLowerCase().indexOf(this.state.keyword.toLowerCase()) > -1
        ||
        data.des_qshe_file.toLowerCase().indexOf(this.state.keyword.toLowerCase()) > -1
      ){
        result.push(data);
      }
    }

    return result;
  }

  detail(name, url){
    Navigation.push(this.props.componentId, {
      component: {
        name: 'PDFScreen',
        passProps: {
          name,
          url,
        }
      }
    });
  }

  renderRow({item, index}){
    return(
      <TouchableOpacity onPress={() => this.detail(item.nama_qshe_file, item.url_qshe_file)}>
        <View style={{alignSelf: 'stretch'}}>
          <View style={{flexDirection: 'row', flex: 1, padding: 8}}>
            <Image source={require('../../assets/images/ic_pdf_icon.png')} style={{width: 50, height: 50, resizeMode: 'contain'}}/>
            <View style={{flex: 1, marginLeft: 8}}>
              <Text style={{fontSize: 15, fontWeight: 'bold'}}>{item.nama_qshe_file}</Text>
              <Text>{item.des_qshe_file}</Text>
            </View>
          </View>
          <View style={{alignSelf: 'stretch', height: 1, backgroundColor: '#ecf0f1'}}/>
        </View>
      </TouchableOpacity>
    );
  }

  render() {
    return (
      <BaseComponent style={styles.container}>
        <View
          style={{alignSelf: 'stretch', flexDirection: 'row', backgroundColor: Colors.PRIMARY_COLOR, padding: 8}}
        >
          <View style={{flex: 1, flexDirection: 'row', borderColor: '#e2574c', backgroundColor: '#e2574c', borderRadius: 6, borderWidth: 2}}>
            <Image source={require('../../assets/images/ic_search.png')} style={{margin: 3}}/>
            <TextInput onChangeText={(keyword) => this.setState({keyword})} autoFocus style={{backgroundColor: '#fff', flex: 1, padding: 5, paddingLeft: 10, paddingRight: 10, borderBottomRightRadius: 5, borderTopRightRadius: 5}} placeholder={'Apa yang anda cari ?'}/>
          </View>
        </View>
        <Text>{this.state.isLoading}</Text>
        <FlatList
          data={this.getData()}
          keyExtractor={(item, index) => `${index}`}
          renderItem={(data) => this.renderRow(data)}
        />
        <OrientationLoadingOverlay
          visible={this.state.isLoading}
          color="white"
          indicatorSize="large"
          messageFontSize={24}
          message="Please wait..."
          />
      </BaseComponent>
    );
  }
}

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});

//make this component available to the app
export default SearchScreen;
