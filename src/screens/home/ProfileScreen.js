//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, ImageBackground, TouchableOpacity, AsyncStorage, TextInput } from 'react-native';
import { Navigation } from 'react-native-navigation';
import { Avatar, Button } from 'react-native-elements';
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
import { TextInputLayout } from 'rn-textinputlayout';
import Colors from '../../utilities/Colors';
import Helpers from '../../utilities/Helpers';
import WikaAPI from '../../WikaAPI';
import BaseComponent from '../../components/BaseComponent';

// create a component
class ProfileScreen extends Component {
  static options(passProps) {
    return {
      topBar: {
        visible: true,
        title: {
          text: 'My Profile',
          color: 'white',
        },
        background: {
          color: Colors.PRIMARY_COLOR,
        },
        backButton: {
          color: '#fff',
          visible: true
        },
      },
      layout: {
        orientation: ['portrait'] // An array of supported orientations
      },
    };
  }

  constructor(props){
    super(props);
    Navigation.events().bindComponent(this);
    this.state = {
      isLoading: false,
      user: null,
      unitKerja: null,
      newPassword: '',
      confirmNewPassword: '',
    };
  }

  componentDidAppear(){
    this.loadDataUser();
    this.loadDataUnitKerja();
  }

  async loadDataUser(){
    const user = await AsyncStorage.getItem('user');
    this.setState({
      user: JSON.parse(user),
    })
  }

  async loadDataUnitKerja(){
    const unitKerja = await AsyncStorage.getItem('unitKerja');
    this.setState({
      unitKerja: JSON.parse(unitKerja),
    })
  }

  async changePassword(){
    if(this.state.newPassword === ''){
      Helpers.message('Whoops', 'Please enter the new password!');
    }else if(this.state.confirmNewPassword === ''){
      Helpers.message('Whoops', 'Please enter the again password!');
    }else{
      if(this.state.newPassword !== this.state.confirmNewPassword){
        Helpers.message('Whoops', 'Password does not match...');
      }else{
        this.setState({isLoading: true});
        const formData = new FormData();
        formData.append('id', this.state.user.id);
        formData.append('password_user', this.state.user.id);
        const response = await WikaAPI.edit_user(formData);
        if(response.data.api_status === 1){
          this.setState({
            newPassword: '',
            confirmNewPassword: '',
          })
          Helpers.message('Success', 'Password has been changed');
        }else{
          Helpers.message('Whoops', response.data.api_message);
        }
        this.setState({isLoading: false});
      }
    }
  }

  render() {
    return (
      <View style={styles.container}>
        {this.state.user !== null &&
          <ImageBackground source={require('../../assets/images/head_profile.png')} style={{alignSelf: 'stretch', height: null, resizeMode: 'cover', alignItems: 'flex-start', justifyContent: 'center', padding: 15}}>
            <Avatar
              rounded
              large
              source={{uri: this.state.user.url_photo_user}}
              activeOpacity={0.7}
            />
            <Text style={{color: '#fff', fontWeight: 'bold', marginTop: 10}}>{this.state.user.nama}</Text>
            <Text style={{color: '#fff', fontSize: 12}}>{this.state.user.email_user}</Text>
          </ImageBackground>
        }
        {this.state.unitKerja !== null && this.state.user !== null &&
          <View style={{padding: 15}}>
            <Text><Text style={{fontWeight: 'bold'}}>NIP: </Text>{this.state.user.nik}</Text>
            <View style={{alignSelf: 'stretch', height: 1, backgroundColor: '#2d2d2d', marginTop: 8, marginBottom: 8}}/>
            <Text><Text style={{fontWeight: 'bold'}}>Unit Kerja: </Text>{this.state.unitKerja.nama_unit_kerja}</Text>
            <View style={{marginTop: 25}}/>
            <TextInputLayout
              focusColor={Colors.PRIMARY_COLOR}
            >
              <TextInput
                autoFocus
                secureTextEntry
                style={{height: 40, fontSize: 14}}
                placeholder={'Enter New Password'}
                value={this.state.newPassword}
                onChangeText={(newPassword) => this.setState({newPassword})}
              />
            </TextInputLayout>
            <TextInputLayout
              style={{marginTop: 5}}
              focusColor={Colors.PRIMARY_COLOR}
            >
              <TextInput
                secureTextEntry
                style={{height: 40, fontSize: 14}}
                placeholder={'Enter Again Password'}
                value={this.state.confirmNewPassword}
                onChangeText={(confirmNewPassword) => this.setState({confirmNewPassword})}
              />
            </TextInputLayout>

            <Button
              onPress={() => this.changePassword()}
              borderRadius={5}
              containerViewStyle={{marginTop: 30, marginLeft: 0, marginRight: 0}}
              title='Save'
              textStyle={{color: '#fff'}}
              backgroundColor={Colors.PRIMARY_COLOR}
            />
          </View>
        }
        <OrientationLoadingOverlay
          visible={this.state.isLoading}
          color="white"
          indicatorSize="large"
          messageFontSize={24}
          message="Please wait..."
          />
      </View>
    );
  }
}

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});

//make this component available to the app
export default ProfileScreen;
