//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, AsyncStorage, TextInput, ScrollView, FlatList } from 'react-native';
import { Navigation } from 'react-native-navigation';
import { Avatar, Button } from 'react-native-elements';
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
import { TextInputLayout } from 'rn-textinputlayout';
import Colors from '../../utilities/Colors';
import Helpers from '../../utilities/Helpers';
import WikaAPI from '../../WikaAPI';
import BaseComponent from '../../components/BaseComponent';

// create a component
class SearchScreen extends Component {
  static options(passProps) {
    return {
      topBar: {
        visible: true,
        title: {
          text: 'Notification',
          color: 'white',
        },
        background: {
          color: Colors.PRIMARY_COLOR,
        },
        backButton: {
          title: 'Back',
          color: '#fff',
          visible: true,
          showTitle: false,
        },
      },
      layout: {
        orientation: ['portrait'] // An array of supported orientations
      },
    };
  }

  constructor(props){
    super(props);
    Navigation.events().bindComponent(this);
    this.state = {
      isLoading: false,
      datas: [],
      keyword: '',
    };
  }

  componentDidMount(){
    this.loadDataNotifications();
  }

  async loadDataNotifications(){
    try{
      const notifications = await AsyncStorage.getItem('notifications');
      this.setState({
        datas: JSON.parse(notifications),
      })
    }catch(error){

    }
  }

  async detail(name, url, index){
    Navigation.push(this.props.componentId, {
      component: {
        name: 'PDFScreen',
        passProps: {
          name,
          url,
        }
      }
    });
    let notifications = Object.assign([], this.state.datas);
    notifications.splice(index, 1);
    await AsyncStorage.setItem('notifications', JSON.stringify(notifications));
    this.setState({datas: notifications});
  }

  renderRow({item, index}){
    return(
      <TouchableOpacity onPress={() => this.detail(item.nama_qshe_file, item.url_qshe_file, index)}>
        <View style={{alignSelf: 'stretch'}}>
          <View style={{flexDirection: 'row', flex: 1, padding: 8}}>
            <Image source={require('../../assets/images/ic_pdf_icon.png')} style={{width: 50, height: 50, resizeMode: 'contain'}}/>
            <View style={{flex: 1, marginLeft: 8}}>
              <Text style={{fontSize: 15, fontWeight: 'bold'}}>{item.nama_qshe_file}</Text>
              <Text>{item.des_qshe_file}</Text>
            </View>
          </View>
          <View style={{alignSelf: 'stretch', height: 1, backgroundColor: '#ecf0f1'}}/>
        </View>
      </TouchableOpacity>
    );
  }

  render() {
    return (
      <BaseComponent style={styles.container}>
        <Text>{this.state.isLoading}</Text>
        <FlatList
          data={this.state.datas}
          keyExtractor={(item, index) => `${index}`}
          renderItem={(data) => this.renderRow(data)}
        />
        <OrientationLoadingOverlay
          visible={this.state.isLoading}
          color="white"
          indicatorSize="large"
          messageFontSize={24}
          message="Please wait..."
          />
      </BaseComponent>
    );
  }
}

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});

//make this component available to the app
export default SearchScreen;
