//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, AsyncStorage, ImageBackground, ScrollView, Dimensions } from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import SideMenu from 'react-native-side-menu';
import { Navigation } from 'react-native-navigation';
import { Header, Avatar } from 'react-native-elements';
import ImageSlider from 'react-native-image-slider';
import MarqueeLabel from 'react-native-lahk-marquee-label';
import Modal from 'react-native-modalbox';
import Emoji from 'react-native-emoji';
import Colors from '../../utilities/Colors';
import Helpers from '../../utilities/Helpers';
import WikaAPI from '../../WikaAPI';
import BaseComponent from '../../components/BaseComponent';

const widthScreen = Dimensions.get('window').width;
const heightBanner = Dimensions.get('window').width * (1/2.3);
// create a component
class HomeScreen extends BaseComponent {
  static options(passProps) {
    return {
      topBar: {
        visible: false,
      },
      layout: {
        orientation: ['portrait'] // An array of supported orientations
      },
    };
  }

  constructor(props){
    super(props);
    Navigation.events().bindComponent(this);
    this.state = {
      isSideMenuOpen: false,
      isFirstShowModalWelcome: true,
      isShowModalWelcome: false,
      user: null,
      banner: [],
      runningText: '',
      countNotification: 0,
    };
  }

  componentDidAppear(){
    this.loadDataUser();
    this.loadDataBanner();
    this.loadRunningtext();
    this.loadDataNotifications();

    if(this.props.isShowModalWelcome !== undefined && this.state.isFirstShowModalWelcome){
      this.setState({
        isShowModalWelcome: true,
        isFirstShowModalWelcome: false,
      })
    }
  }

  async loadDataNotifications(){
    try{
      const notifications = await AsyncStorage.getItem('notifications');
      this.setState({
        countNotification: JSON.parse(notifications).length,
      })
    }catch(error){

    }
  }

  async loadDataBanner(){
    this.setState({banner: []});
    const response = await WikaAPI.banner_slider();
    let resultBanner = [];
    for(let banner of response.data.data){
      resultBanner.push(banner.url_banner_img);
    }
    this.setState({banner: resultBanner});
  }

  async loadRunningtext(){
    this.setState({banner: []});
    const response = await WikaAPI.running_text();
    this.setState({runningText: response.data.content_text_running});
  }

  async loadDataUser(){
    const user = await AsyncStorage.getItem('user');
    await this.setState({
      user: JSON.parse(user),
    })
    this.loadDataUnitKerja();
  }

  async loadDataUnitKerja(){
    const formData = new FormData();
    formData.append('id', this.state.user.unit_kerja_id);

    const response = await WikaAPI.get_unit_kerja(formData);
    await AsyncStorage.setItem('unitKerja', JSON.stringify(response.data));
  }

  goToProfileScreen(){
    this.setState({isSideMenuOpen: false});
    Navigation.push(this.props.componentId, {
      component: {
        name: 'ProfileScreen',
      }
    });
  }

  goToNotificationScreen(){
    this.setState({isSideMenuOpen: false});
    Navigation.push(this.props.componentId, {
      component: {
        name: 'NotificationScreen',
      }
    });
  }

  goToLogout(){
    AsyncStorage.removeItem('user');
    this.setState({isSideMenuOpen: false});
    Navigation.setStackRoot(this.props.componentId, {
      component: {
        name: 'LoginScreen',
      }
    });
  }

  goToSearchScreen(){
    Navigation.push(this.props.componentId, {
      component: {
        name: 'SearchScreen',
      }
    });
  }

  goToQualityScreen(){
    Navigation.push(this.props.componentId, {
      component: {
        name: 'QualityScreen',
      }
    });
  }

  goToSafetyScreen(){
    Navigation.push(this.props.componentId, {
      component: {
        name: 'ListDataScreen',
        passProps: {
          name: 'Safety',
          parentName: 'Safety',
          params: [
            {
              name: 'master_modul_id',
              data: 2
            }
          ]
        }
      }
    });
  }

  goToReportScreen(){
    Navigation.push(this.props.componentId, {
      component: {
        name: 'ReportScreen',
      }
    });
  }

  goToWRInfoScreen(){
    Navigation.push(this.props.componentId, {
      component: {
        name: 'WRInfoScreen',
      }
    });
  }

  renderLeftSidebar(){
    return(
      <View style={{backgroundColor: Colors.PRIMARY_COLOR, flex: 1, borderRightColor: '#95ba9c', borderRightWidth: 1}}>
        {this.state.user !== null &&
          <View style={{marginTop: 50, backgroundColor: '#fff', flex: 1}}>
            <View style={{backgroundColor: Colors.PRIMARY_COLOR, padding: 10, marginBottom: 15}}>
              <Avatar
                onPress={() => this._drawer.close()}
                rounded
                large
                source={{uri: this.state.user.url_photo_user}}
                activeOpacity={0.7}
              />
              <Text style={{color: '#fff', fontWeight: 'bold', marginTop: 10}}>{this.state.user.nama}</Text>
              <Text style={{color: '#fff', fontSize: 12}}>{this.state.user.email_user}</Text>
            </View>
            <TouchableOpacity onPress={() => this.goToProfileScreen()}>
              <View style={{flexDirection: 'row', alignItems: 'center', padding: 10, paddingLeft: 15, paddingRight: 15}}>
                <Image source={require('../../assets/images/ic_profile.png')} style={{width: 25, height: 25}}/>
                <Text style={{marginLeft: 20}}>Profile</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.goToNotificationScreen()}>
              <View style={{flexDirection: 'row', alignItems: 'center', padding: 10, paddingLeft: 15, paddingRight: 15}}>
                <Image source={require('../../assets/images/ic_notification.png')} style={{width: 25, height: 25}}/>
                <Text style={{marginLeft: 20, flex: 1}}>Notification</Text>
                <Text style={{color: 'red'}}>{this.state.countNotification}</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.goToLogout()}>
              <View style={{flexDirection: 'row', alignItems: 'center', padding: 10, paddingLeft: 15, paddingRight: 15}}>
                <Image source={require('../../assets/images/ic_logout.png')} style={{width: 25, height: 25}}/>
                <Text style={{marginLeft: 20}}>Logout</Text>
              </View>
            </TouchableOpacity>
          </View>
        }
      </View>
    )
  }

  render() {
    return (
      <SideMenu
        isOpen={this.state.isSideMenuOpen}
        bounceBackOnOverdraw={false}
				menu={ this.renderLeftSidebar() }
        onChange={(isSideMenuOpen) => this.setState({isSideMenuOpen})}>
          <Header
            backgroundColor={Colors.PRIMARY_COLOR}
            leftComponent={{ icon: 'bars', color: '#fff', onPress: () => this.setState({isSideMenuOpen: true}), type: 'font-awesome'}}
            centerComponent={{ text: 'WIKA REALTY', style: { color: '#fff', fontWeight: 'bold', textAlign: 'left'} }}
            rightComponent={{ icon: 'search', color: '#fff', onPress: () => this.goToSearchScreen(), type: 'font-awesome'}}
          />
          <ImageBackground style={styles.container} source={require('../../assets/images/bg_menu.png')}>
          <ScrollView style={{alignSelf: 'stretch', flex: 1}}>
            <ImageSlider
              style={{height: heightBanner}}
              loopBothSides
              autoPlayWithInterval={3000}
              images={this.state.banner}
            />
            <View style={{flex: 3, alignSelf: 'stretch', padding: 10}}>
              <View style={{flexDirection: 'row'}}>
                <TouchableOpacity
                  onPress={() => this.goToQualityScreen()}
                  style={{flex: 1, backgroundColor: '#2fae72', marginBottom: 5, marginRight: 5, alignItems: 'center', justifyContent: 'center', paddingTop: 25, paddingBottom: 25}}
                >
                  <View style={{alignItems: 'center', justifyContent: 'center'}}>
                    <Image source={require('../../assets/images/kualitas_menu.png')}/>
                    <Text style={{fontWeight: 'bold', color: '#fff'}}>QUALITY</Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => this.goToSafetyScreen()}
                  style={{flex: 1, backgroundColor: '#2fae72', marginBottom: 5, marginLeft: 5, alignItems: 'center', justifyContent: 'center', paddingTop: 25, paddingBottom: 25}}
                >
                  <View style={{alignItems: 'center', justifyContent: 'center'}}>
                    <Image source={require('../../assets/images/safety_menu.png')}/>
                    <Text style={{fontWeight: 'bold', color: '#fff'}}>SAFETY</Text>
                  </View>
                </TouchableOpacity>
              </View>
              <View style={{flexDirection: 'row'}}>
                <TouchableOpacity
                  onPress={() => this.goToReportScreen()}
                  style={{flex: 1, backgroundColor: '#2fae72', marginTop: 5, marginRight: 5, alignItems: 'center', justifyContent: 'center', paddingTop: 25, paddingBottom: 25}}
                >
                  <View style={{alignItems: 'center', justifyContent: 'center'}}>
                    <Image source={require('../../assets/images/laporan_menu.png')}/>
                    <Text style={{fontWeight: 'bold', color: '#fff'}}>REPORT</Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => this.goToWRInfoScreen()}
                  style={{flex: 1, backgroundColor: '#2fae72', marginTop: 5, marginLeft: 5, alignItems: 'center', justifyContent: 'center', paddingTop: 25, paddingBottom: 25}}
                >
                  <View style={{alignItems: 'center', justifyContent: 'center'}}>
                    <Image source={require('../../assets/images/process_info_wr.png')}/>
                    <Text style={{fontWeight: 'bold', color: '#fff'}}>WR INFO</Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </ScrollView>
          <View style={{backgroundColor: '#95ba9c', padding: 8, flexDirection: 'row', alignSelf: 'stretch'}}>
            <Image source={require('../../assets/images/ic_message.png')} style={{width: 30, height: 20, resizeMode: 'contain'}}/>
            <MarqueeLabel
              duration={30000}
              text={this.state.runningText}
              textStyle={{ fontSize: 13, color: '#fff'}}
              textContainerStyle={{height: null}}
            />
          </View>
          <Modal
            isOpen={this.state.isShowModalWelcome}
            onClosed={() => this.setState({isShowModalWelcome: false})} 
            style={{justifyContent: 'center', alignItems: 'center', width: widthScreen - 50, height: null, paddingLeft: 0, paddingRight: 0}} position={"center"}
          >
            <View style={{alignSelf: 'stretch', height: null}}>
              <ImageBackground source={require('../../assets/images/head_profile.png')} style={{alignSelf: 'stretch', height: 125, resizeMode: 'cover', alignItems: 'center', justifyContent: 'center'}}>
                <Text style={{color: '#fff', fontSize: 25}}>Welcome !</Text>
              </ImageBackground>
              <View style={{padding: 22}}>
                <Text>{this.state.user ? this.state.user.nama: '-'}</Text>
                <Text style={{marginTop: 15}}>Curlture of Quality & Safety it starts with you <Emoji name={':blush:'}/></Text>
                <View style={{marginTop: 20, alignItems: 'flex-end', justifyContent: 'flex-end',}}>
                  <TouchableOpacity onPress={() => this.setState({isShowModalWelcome: false})}>
                    <Text style={{color: '#3498db', fontWeight: 'bold'}}>Ok</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </Modal>
        </ImageBackground>
      </SideMenu>
    );
  }
}

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
});

//make this component available to the app
export default HomeScreen;
