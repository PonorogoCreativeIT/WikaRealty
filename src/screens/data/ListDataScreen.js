//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, ImageBackground, Dimensions, TouchableOpacity, FlatList } from 'react-native';
import Pdf from 'react-native-pdf';
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';
import { Navigation } from 'react-native-navigation';
import Colors from '../../utilities/Colors';
import BaseComponent from '../../components/BaseComponent';
import Helpers from '../../utilities/Helpers';
import WikaAPI from '../../WikaAPI';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
// create a component
class SafetyScreen extends Component {
  static options(passProps) {
    return {
      topBar: {
        visible: true,
        title: {
          color: 'white',
        },
        background: {
          color: Colors.PRIMARY_COLOR,
        },
        backButton: {
          title: 'Back',
          color: '#fff',
          visible: true,
          showTitle: false,
        },
        rightButtons: [
          {
            id: 'home',
            icon: require('../../assets/images/ic_home.png')
          }
        ],
      },
      layout: {
        orientation: ['portrait'] // An array of supported orientations
      },
    };
  }

  constructor(props){
    super(props);
    Navigation.events().bindComponent(this);
    Navigation.mergeOptions(this.props.componentId, {
      topBar: {
        title: {
          text: this.props.name ? this.props.name : 'Detail',
        }
      }
    });

    this.state = {
      type: 'module',
      isLoading: false,
      isDataNotFound: false,
    };
  }

  componentDidMount(){
    this.loadDataModules();
  }

  async loadDataModules(){
    try{
      this.setState({
        datas: [],
      });
      if(this.props.params.length === 1){
        const formData = new FormData();
        for(let param of this.props.params){
          formData.append(`${param.name}`, `${param.data}`);
        }
        const response = await WikaAPI.get_sub_modul_i(formData);
        this.setState({
          isLoading: false,
          datas: response.data.data,
        })

        if(this.state.datas.length === 0){
          this.loadPDFs();
        }
      }else if(this.props.params.length === 5){
        const formData = new FormData();
        for(let param of this.props.params){
          formData.append(`${param.name}`, `${param.data}`);
        }
        const response = await WikaAPI.get_sub_modul_ii(formData);
        this.setState({
          isLoading: false,
          datas: response.data.data,
        })

        if(this.state.datas.length === 0){
          this.loadPDFs();
        }
      }else if(this.props.params.length === 6){
        const formData = new FormData();
        for(let param of this.props.params){
          formData.append(`${param.name}`, `${param.data}`);
        }
        const response = await WikaAPI.get_sub_modul_iii(formData);
        this.setState({
          isLoading: false,
          datas: response.data.data,
        })

        if(this.state.datas.length === 0){
          this.loadPDFs();
        }
      }else if(this.props.params.length === 7){
        const formData = new FormData();
        for(let param of this.props.params){
          formData.append(`${param.name}`, `${param.data}`);
        }
        const response = await WikaAPI.get_sub_modul_iv(formData);
        this.setState({
          isLoading: false,
          datas: response.data.data,
        })

        if(this.state.datas.length === 0){
          this.loadPDFs();
        }
      }else if(this.props.params.length === 8){
        const formData = new FormData();
        for(let param of this.props.params){
          formData.append(`${param.name}`, `${param.data}`);
        }
        const response = await WikaAPI.get_sub_modul_v(formData);
        this.setState({
          isLoading: false,
          datas: response.data.data,
        })

        if(this.state.datas.length === 0){
          this.loadPDFs();
        }
      }else{
        this.setState({
          isDataNotFound: true,
        })
      }
    }catch(error){
      this.setState({
        isLoading: false,
      });
      Helpers.message(JSON.stringify(error));
    }
  }

  async loadPDFs(){
    try{
      this.setState({
        datas: [],
        type: 'files',
      });
      if(this.props.params.length === 5){
        const formData = new FormData();
        for(let param of this.props.params){
          let paramName = '';
          if(param.name === 'master_modul_id'){
            paramName = 'master_module_id';
          }else{
            paramName = param.name;
          }
          formData.append(`${paramName}`, `${param.data}`);
        }
        const response = await WikaAPI.get_file_pdf_qshe_i(formData);
        this.setState({
          isLoading: false,
          datas: response.data.data,
        })
      }else if(this.props.params.length === 6){
        const formData = new FormData();
        for(let param of this.props.params){
          let paramName = '';
          if(param.name === 'master_modul_id'){
            paramName = 'master_module_id';
          }else{
            paramName = param.name;
          }
          formData.append(`${paramName}`, `${param.data}`);
        }
        const response = await WikaAPI.get_file_pdf_qshe_ii(formData);
        this.setState({
          isLoading: false,
          datas: response.data.data,
        })
      }else if(this.props.params.length === 7){
        const formData = new FormData();
        for(let param of this.props.params){
          let paramName = '';
          if(param.name === 'master_modul_id'){
            paramName = 'master_module_id';
          }else{
            paramName = param.name;
          }
          formData.append(`${paramName}`, `${param.data}`);
        }
        const response = await WikaAPI.get_file_pdf_qshe_iii(formData);
        this.setState({
          isLoading: false,
          datas: response.data.data,
        })
      }else if(this.props.params.length === 8){
        const formData = new FormData();
        for(let param of this.props.params){
          let paramName = '';
          if(param.name === 'master_modul_id'){
            paramName = 'master_module_id';
          }else{
            paramName = param.name;
          }
          formData.append(`${paramName}`, `${param.data}`);
        }
        const response = await WikaAPI.get_file_pdf_qshe_iv(formData);
        this.setState({
          isLoading: false,
          datas: response.data.data,
        })
      }else if(this.props.params.length === 8){
        const formData = new FormData();
        for(let param of this.props.params){
          let paramName = '';
          if(param.name === 'master_modul_id'){
            paramName = 'master_module_id';
          }else{
            paramName = param.name;
          }
          formData.append(`${paramName}`, `${param.data}`);
        }
        const response = await WikaAPI.get_file_pdf_qshe_v(formData);
        this.setState({
          isLoading: false,
          datas: response.data.data,
        })
      }else{
        this.setState({isDataNotFound: true});
      }

      if(this.state.datas.length === 0){
        this.setState({isDataNotFound: true});
      }
    }catch(error){
      this.setState({
        isLoading: false,
      });
      Helpers.message(JSON.stringify(error));
    }
  }

  navigationButtonPressed({ buttonId }) {
    if(buttonId === 'home'){
      Navigation.setStackRoot(this.props.componentId, {
        component: {
          name: 'HomeScreen',
        }
      });
    }
  }

  detailFile(name, url){
    Navigation.push(this.props.componentId, {
      component: {
        name: 'PDFScreen',
        passProps: {
          name,
          url,
        }
      }
    });
  }

  getParams(datas){
    let result = [];
    for(let key in datas){
      let tempResult = {};
      if(Object.keys(datas).length === 5 && key === 'id'){
        tempResult.name = 'sub_modul_i_id';
      }else if(Object.keys(datas).length === 6 && key === 'id'){
        tempResult.name = 'sub_modul_ii_id';
      }else if(Object.keys(datas).length === 7 && key === 'id'){
        tempResult.name = 'sub_modul_iii_id';
      }else if(Object.keys(datas).length === 8 && key === 'id'){
        tempResult.name = 'sub_modul_iv_id';
      }else if(Object.keys(datas).length === 9 && key === 'id'){
        tempResult.name = 'sub_modul_v_id';
      }else{
        tempResult.name = key;
      }
      tempResult.data = datas[key];
      result.push(tempResult);
    }
    return result;
  }

  detailModule(item){
    // alert(JSON.stringify(this.getParams(item)))
    let name = '';
    if(Object.keys(item).length === 5){
      name = item.nama_sub_modul_i;
    }else if(Object.keys(item).length === 6){
      name = item.nama_sub_modul_ii;
    }else if(Object.keys(item).length === 7){
      name = item.nama_sub_modul_iii;
    }else if(Object.keys(item).length === 8){
      name = item.nama_sub_modul_iv;
    }else if(Object.keys(item).length === 9){
      name = item.nama_sub_modul_v;
    }
    Navigation.push(this.props.componentId, {
      component: {
        name: 'ListDataScreen',
        passProps: {
          name,
          parentName: this.props.name,
          params: this.getParams(item),
        }
      }
    });
  }

  renderRow({item, index}){
    if(this.state.type === 'module'){
      if(Object.keys(item).length === 5){
        return(
          <TouchableOpacity onPress={() => this.detailModule(item)}>
            <View style={{alignSelf: 'stretch'}}>
              <View style={{flexDirection: 'row', flex: 1, padding: 8}}>
                <Image source={{uri: item.img_sub_modul_i}} style={{width: 50, height: 50, resizeMode: 'contain'}}/>
                <View style={{flex: 1, marginLeft: 8}}>
                  <Text style={{fontSize: 15, fontWeight: 'bold'}}>{item.nama_sub_modul_i}</Text>
                  <Text>{item.des_sub_modul_i}</Text>
                </View>
              </View>
              <View style={{alignSelf: 'stretch', height: 1, backgroundColor: '#ecf0f1'}}/>
            </View>
          </TouchableOpacity>
        );
      }else if(Object.keys(item).length === 6){
        return(
          <TouchableOpacity onPress={() => this.detailModule(item)}>
            <View style={{alignSelf: 'stretch'}}>
              <View style={{flexDirection: 'row', flex: 1, padding: 8}}>
                <Image source={{uri: item.img_sub_modul_ii}} style={{width: 50, height: 50, resizeMode: 'contain'}}/>
                <View style={{flex: 1, marginLeft: 8}}>
                  <Text style={{fontSize: 15, fontWeight: 'bold'}}>{item.nama_sub_modul_ii}</Text>
                  <Text>{item.des_sub_modul_ii}</Text>
                </View>
              </View>
              <View style={{alignSelf: 'stretch', height: 1, backgroundColor: '#ecf0f1'}}/>
            </View>
          </TouchableOpacity>
        );
      }else if(Object.keys(item).length === 7){
        return(
          <TouchableOpacity onPress={() => this.detailModule(item)}>
            <View style={{alignSelf: 'stretch'}}>
              <View style={{flexDirection: 'row', flex: 1, padding: 8}}>
                <Image source={{uri: item.img_sub_modul_iii}} style={{width: 50, height: 50, resizeMode: 'contain'}}/>
                <View style={{flex: 1, marginLeft: 8}}>
                  <Text style={{fontSize: 15, fontWeight: 'bold'}}>{item.nama_sub_modul_iii}</Text>
                  <Text>{item.des_sub_modul_iii}</Text>
                </View>
              </View>
              <View style={{alignSelf: 'stretch', height: 1, backgroundColor: '#ecf0f1'}}/>
            </View>
          </TouchableOpacity>
        );
      }else if(Object.keys(item).length === 8){
        return(
          <TouchableOpacity onPress={() => this.detailModule(item)}>
            <View style={{alignSelf: 'stretch'}}>
              <View style={{flexDirection: 'row', flex: 1, padding: 8}}>
                <Image source={{uri: item.img_sub_modul_iv}} style={{width: 50, height: 50, resizeMode: 'contain'}}/>
                <View style={{flex: 1, marginLeft: 8}}>
                  <Text style={{fontSize: 15, fontWeight: 'bold'}}>{item.nama_sub_modul_iv}</Text>
                  <Text>{item.des_sub_modul_iv}</Text>
                </View>
              </View>
              <View style={{alignSelf: 'stretch', height: 1, backgroundColor: '#ecf0f1'}}/>
            </View>
          </TouchableOpacity>
        );
      }else if(Object.keys(item).length === 9){
        return(
          <TouchableOpacity onPress={() => this.detailModule(item)}>
            <View style={{alignSelf: 'stretch'}}>
              <View style={{flexDirection: 'row', flex: 1, padding: 8}}>
                <Image source={{uri: item.img_sub_modul_v}} style={{width: 50, height: 50, resizeMode: 'contain'}}/>
                <View style={{flex: 1, marginLeft: 8}}>
                  <Text style={{fontSize: 15, fontWeight: 'bold'}}>{item.nama_sub_modul_v}</Text>
                  <Text>{item.des_sub_modul_v}</Text>
                </View>
              </View>
              <View style={{alignSelf: 'stretch', height: 1, backgroundColor: '#ecf0f1'}}/>
            </View>
          </TouchableOpacity>
        );
      }
    }else{
      return(
        <TouchableOpacity onPress={() => this.detailFile(item.nama_qshe_file, item.url_qshe_file)}>
          <View style={{alignSelf: 'stretch'}}>
            <View style={{flexDirection: 'row', flex: 1, padding: 8}}>
              <Image source={require('../../assets/images/ic_pdf_icon.png')} style={{width: 50, height: 50, resizeMode: 'contain'}}/>
              <View style={{flex: 1, marginLeft: 8}}>
                <Text style={{fontSize: 15, fontWeight: 'bold'}}>{item.nama_qshe_file}</Text>
                <Text>{item.des_qshe_file}</Text>
              </View>
            </View>
            <View style={{alignSelf: 'stretch', height: 1, backgroundColor: '#ecf0f1'}}/>
          </View>
        </TouchableOpacity>
      );
    }
  }

  render() {
    return (
      <BaseComponent>
        <View style={styles.container}>
          <View
            style={{backgroundColor: '#bfbfbf', padding: 5, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start'}}
          >
            <Image source={require('../../assets/images/kualitas_nav.png')} style={{width: 20, height: 20, resizeMode: 'contain'}}/>
            {this.props.parentName === this.props.name ?
              <View style={{flexDirection: 'row'}}>
                <Text style={{fontWeight: 'bold', fontSize: 13, marginLeft: 15, marginTop: 2}}>{this.props.parentName}</Text>
              </View>
              :
              <View style={{flexDirection: 'row'}}>
                <Text style={{fontWeight: 'bold', fontSize: 13, marginLeft: 15, marginTop: 2, color: Colors.PRIMARY_COLOR}}>{this.props.parentName}</Text>
                <Text>&nbsp;►&nbsp;</Text>
                <Text style={{fontWeight: 'bold', fontSize: 13, marginTop: 2, flex: 1}}>{this.props.name}</Text>
              </View>
            }
            
          </View>
          {this.state.isDataNotFound === false &&
            <FlatList
              data={this.state.datas}
              keyExtractor={(item, index) => `${index}`}
              renderItem={(data) => this.renderRow(data)}
            />
          }
          {this.state.isDataNotFound === true &&
            <Image source={require('../../assets/images/no_found_file.png')} style={{flex: 1, width: screenWidth, resizeMode: 'contain'}}/>
          }
          <OrientationLoadingOverlay
            visible={this.state.isLoading}
            color="white"
            indicatorSize="large"
            messageFontSize={24}
            message="Please wait..."
            />
        </View>
      </BaseComponent>
    );
  }
}

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});

//make this component available to the app
export default SafetyScreen;
