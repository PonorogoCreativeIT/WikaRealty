//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, Dimensions, AsyncStorage } from 'react-native';
import Pdf from 'react-native-pdf';
import { Navigation } from 'react-native-navigation';
import Colors from '../../utilities/Colors';
import BaseComponent from '../../components/BaseComponent';
import WikaAPI from '../../WikaAPI';
import Helpers from '../../utilities/Helpers';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
// create a component
class PDFScreen extends Component {
  static options(passProps) {
    return {
      topBar: {
        visible: true,
        title: {
          text: 'Detail PDF',
          color: 'white',
        },
        background: {
          color: Colors.PRIMARY_COLOR,
        },
        backButton: {
          title: 'Back',
          color: '#fff',
          visible: true,
          showTitle: false,
        },
        rightButtons: [
          {
            id: 'home',
            icon: require('../../assets/images/ic_home.png')
          }
        ],
      },
      layout: {
        orientation: ['portrait'] // An array of supported orientations
      },
    };
  }

  constructor(props){
    super(props);
    Navigation.events().bindComponent(this);

    Navigation.mergeOptions(this.props.componentId, {
      topBar: {
        title: {
          text: this.props.name ? this.props.name : 'Detail PDF',
        }
      }
    });
  }

  navigationButtonPressed({ buttonId }) {
    if(buttonId === 'home'){
      Navigation.setStackRoot(this.props.componentId, {
        component: {
          name: 'HomeScreen',
        }
      });
    }
  }

  componentDidAppear(){
    this.loadDataUser();
  }

  async loadDataUser(){
    const user = await AsyncStorage.getItem('user');
    this.setUserLog(JSON.parse(user));
  }

  async setUserLog(user){
    const formData = new FormData();
    formData.append("nip_user_mobile", user.nik);
    formData.append("nama_user_mobile", user.nama);
    formData.append("unit_kerja_user", user.unit_kerja_id);
    formData.append("aktifitas_log_user", this.props.name);
    const response = await WikaAPI.log_user_mobile(formData);
  }

  render() {
    return (
      <BaseComponent style={styles.container}>
        <Pdf
          source={{uri:this.props.url,cache:true}}
          style={{flex: 1, backgroundColor: '#fff'}}/>
        <Image
          pointerEvents="none"
          style={{position: 'absolute', top: 0, left: 0, bottom: 0, right: 0, flex: 1, width: screenWidth, resizeMode: 'contain', alignItems: 'center', justifyContent: 'center'}}
          source={require('../../assets/images/watermark_bg.png')}
        />
      </BaseComponent>
    );
  }
}

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});

//make this component available to the app
export default PDFScreen;
