//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, ImageBackground, Dimensions, TouchableOpacity, ScrollView } from 'react-native';
import Pdf from 'react-native-pdf';
import { Navigation } from 'react-native-navigation';
import Colors from '../../utilities/Colors';
import BaseComponent from '../../components/BaseComponent';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
// create a component
class SafetyScreen extends Component {
  static options(passProps) {
    return {
      topBar: {
        visible: true,
        title: {
          text: 'Safety',
          color: 'white',
        },
        background: {
          color: Colors.PRIMARY_COLOR,
        },
        backButton: {
          color: '#fff',
          visible: true
        },
        rightButtons: [
          {
            id: 'home',
            icon: require('../../assets/images/ic_home.png')
          }
        ],
      },
      layout: {
        orientation: ['portrait'] // An array of supported orientations
      },
    };
  }

  constructor(props){
    super(props);
    Navigation.events().bindComponent(this);
  }

  navigationButtonPressed({ buttonId }) {
    if(buttonId === 'home'){
      Navigation.setStackRoot(this.props.componentId, {
        component: {
          name: 'HomeScreen',
        }
      });
    }
  }

  render() {
    return (
      <BaseComponent>
        <ImageBackground style={styles.container} source={require('../../assets/images/bg_menu.png')}>
          <View
            style={{backgroundColor: '#bfbfbf', padding: 5, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start'}}
          >
            <Image source={require('../../assets/images/kualitas_nav.png')} style={{width: 20, height: 20, resizeMode: 'contain'}}/>
            <Text style={{fontWeight: 'bold', fontSize: 13, marginLeft: 15, flex: 1, marginTop: 2}}>Safety</Text>
          </View>
          <ScrollView style={{flex: 3, alignSelf: 'stretch', padding: 10}}>
            <View style={{flexDirection: 'row'}}>
              <TouchableOpacity
                onPress={() => this.goToSafetyScreen()}
                style={{flex: 1, backgroundColor: '#57ace6', marginBottom: 5, marginRight: 5, alignItems: 'center', justifyContent: 'center', paddingTop: 25, paddingBottom: 25}}
              >
                <View style={{alignItems: 'center', justifyContent: 'center'}}>
                  <Image source={require('../../assets/images/high_rise_menu1.png')}/>
                  <Text style={{fontWeight: 'bold', color: '#fff'}}>HIGH RISE</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.goToSafetyScreen()}
                style={{flex: 1, backgroundColor: '#57ace6', marginBottom: 5, marginLeft: 5, alignItems: 'center', justifyContent: 'center', paddingTop: 25, paddingBottom: 25}}
              >
                <View style={{alignItems: 'center', justifyContent: 'center'}}>
                  <Image source={require('../../assets/images/landed_menu.png')}/>
                  <Text style={{fontWeight: 'bold', color: '#fff'}}>LANDED</Text>
                </View>
              </TouchableOpacity>
            </View>
            <View style={{flexDirection: 'row'}}>
              <TouchableOpacity
                onPress={() => this.goToReportScreen()}
                style={{flex: 1, backgroundColor: '#57ace6', marginTop: 5, marginRight: 5, alignItems: 'center', justifyContent: 'center', paddingTop: 25, paddingBottom: 25}}
              >
                <View style={{alignItems: 'center', justifyContent: 'center'}}>
                  <Image source={require('../../assets/images/properti_satu_menu.png')}/>
                  <Text style={{fontWeight: 'bold', color: '#fff'}}>PROPERTY 1</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.goToWRInfoScreen()}
                style={{flex: 1, backgroundColor: '#57ace6', marginTop: 5, marginLeft: 5, alignItems: 'center', justifyContent: 'center', paddingTop: 25, paddingBottom: 25}}
              >
                <View style={{alignItems: 'center', justifyContent: 'center'}}>
                  <Image source={require('../../assets/images/properti_dua_menu.png')}/>
                  <Text style={{fontWeight: 'bold', color: '#fff'}}>PROPERTY 2</Text>
                </View>
              </TouchableOpacity>
            </View>
          </ScrollView>
        </ImageBackground>
      </BaseComponent>
    );
  }
}

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});

//make this component available to the app
export default SafetyScreen;
