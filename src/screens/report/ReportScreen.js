//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, ImageBackground, Dimensions, TouchableOpacity, ScrollView } from 'react-native';
import Pdf from 'react-native-pdf';
import { Navigation } from 'react-native-navigation';
import Colors from '../../utilities/Colors';
import BaseComponent from '../../components/BaseComponent';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
// create a component
class ReportScreen extends Component {
  static options(passProps) {
    return {
      topBar: {
        visible: true,
        title: {
          text: 'Report',
          color: 'white',
        },
        background: {
          color: Colors.PRIMARY_COLOR,
        },
        backButton: {
          color: '#fff',
          visible: true
        },
        rightButtons: [
          {
            id: 'home',
            icon: require('../../assets/images/ic_home.png')
          }
        ],
      },
      layout: {
        orientation: ['portrait'] // An array of supported orientations
      },
    };
  }

  constructor(props){
    super(props);
    Navigation.events().bindComponent(this);
  }

  navigationButtonPressed({ buttonId }) {
    if(buttonId === 'home'){
      Navigation.setStackRoot(this.props.componentId, {
        component: {
          name: 'HomeScreen',
        }
      });
    }
  }

  goToK35R(){
    Navigation.push(this.props.componentId, {
      component: {
        name: 'ListDataScreen',
        passProps: {
          name: 'K3 & 5R',
          parentName: 'Report',
          params: [
            {
              name: 'nama_sub_modul_i',
              data: '3.1 K3 & 5R',
            },
            {
              name: 'des_sub_modul_i',
              data: 'Deskripsi singkat sub modul'
            },
            {
              name: 'img_sub_modul_i',
              data: 'http://202.59.167.149/uploads/1/2018-03/k3_menu.png'
            },
            {
              name: 'master_modul_id',
              data: 3
            },
            {
              name: 'sub_modul_i_id',
              data: 9
            },
          ]
        }
      }
    });
  }

  goToQPASS(){
    Navigation.push(this.props.componentId, {
      component: {
        name: 'ListDataScreen',
        passProps: {
          name: 'QPASS',
          parentName: 'Report',
          params: [
            {
              name: 'nama_sub_modul_i',
              data: '3.2 QPASS PPU',
            },
            {
              name: 'des_sub_modul_i',
              data: 'Deskripsi singkat sub modul'
            },
            {
              name: 'img_sub_modul_i',
              data: 'http://202.59.167.149/uploads/1/2018-03/qpass_menu.png'
            },
            {
              name: 'master_modul_id',
              data: 3
            },
            {
              name: 'sub_modul_i_id',
              data: 10
            },
          ]
        }
      }
    });
  }

  goToAuditInternal(){
    Navigation.push(this.props.componentId, {
      component: {
        name: 'ListDataScreen',
        passProps: {
          name: 'Audit Internal',
          parentName: 'Report',
          params: [
            {
              name: 'nama_sub_modul_i',
              data: '3.3 Audit Internal',
            },
            {
              name: 'des_sub_modul_i',
              data: 'Berikut adalah sub module Audit Internal'
            },
            {
              name: 'img_sub_modul_i',
              data: 'http://202.59.167.149/uploads/4/2018-08/images.jpg'
            },
            {
              name: 'master_modul_id',
              data: 3
            },
            {
              name: 'sub_modul_i_id',
              data: 14
            },
          ]
        }
      }
    });
  }

  goToAuditExternal(){
    Navigation.push(this.props.componentId, {
      component: {
        name: 'ListDataScreen',
        passProps: {
          name: 'Audit External',
          parentName: 'Report',
          params: [
            {
              name: 'nama_sub_modul_i',
              data: '3.4 Audit External',
            },
            {
              name: 'des_sub_modul_i',
              data: 'Berikut adalah sub module Audit External'
            },
            {
              name: 'img_sub_modul_i',
              data: 'http://202.59.167.149/uploads/4/2018-08/images.jpg'
            },
            {
              name: 'master_modul_id',
              data: 3
            },
            {
              name: 'sub_modul_i_id',
              data: 15
            },
          ]
        }
      }
    });
  }

  render() {
    return (
      <BaseComponent>
        <ImageBackground style={styles.container} source={require('../../assets/images/bg_menu.png')}>
          <View
            style={{backgroundColor: '#bfbfbf', padding: 5, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start'}}
          >
            <Image source={require('../../assets/images/kualitas_nav.png')} style={{width: 20, height: 20, resizeMode: 'contain'}}/>
            <Text style={{fontWeight: 'bold', fontSize: 13, marginLeft: 15, flex: 1, marginTop: 2}}>Report</Text>
          </View>
          <ScrollView style={{flex: 3, alignSelf: 'stretch', padding: 10}}>
            <View style={{flexDirection: 'row'}}>
              <TouchableOpacity
                onPress={() => this.goToK35R()}
                style={{flex: 1, backgroundColor: '#fccc39', marginBottom: 5, marginRight: 5, alignItems: 'center', justifyContent: 'center', paddingTop: 25, paddingBottom: 25}}
              >
                <View style={{alignItems: 'center', justifyContent: 'center'}}>
                  <Image source={require('../../assets/images/high_rise_menu1.png')}/>
                  <Text style={{fontWeight: 'bold', color: '#fff'}}>K3 & 5R</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.goToQPASS()}
                style={{flex: 1, backgroundColor: '#fccc39', marginBottom: 5, marginLeft: 5, alignItems: 'center', justifyContent: 'center', paddingTop: 25, paddingBottom: 25}}
              >
                <View style={{alignItems: 'center', justifyContent: 'center'}}>
                  <Image source={require('../../assets/images/landed_menu.png')}/>
                  <Text style={{fontWeight: 'bold', color: '#fff'}}>QPASS</Text>
                </View>
              </TouchableOpacity>
            </View>
            <View style={{flexDirection: 'row'}}>
              <TouchableOpacity
                onPress={() => this.goToAuditInternal()}
                style={{flex: 1, backgroundColor: '#fccc39', marginTop: 5, marginRight: 5, alignItems: 'center', justifyContent: 'center', paddingTop: 25, paddingBottom: 25}}
              >
                <View style={{alignItems: 'center', justifyContent: 'center'}}>
                  <Image source={require('../../assets/images/properti_satu_menu.png')}/>
                  <Text style={{fontWeight: 'bold', color: '#fff'}}>AUDIT INTERNAL</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.goToAuditExternal()}
                style={{flex: 1, backgroundColor: '#fccc39', marginTop: 5, marginLeft: 5, alignItems: 'center', justifyContent: 'center', paddingTop: 25, paddingBottom: 25}}
              >
                <View style={{alignItems: 'center', justifyContent: 'center'}}>
                  <Image source={require('../../assets/images/properti_dua_menu.png')}/>
                  <Text style={{fontWeight: 'bold', color: '#fff'}}>AUDIT EXTERNAL</Text>
                </View>
              </TouchableOpacity>
            </View>
          </ScrollView>
        </ImageBackground>
      </BaseComponent>
    );
  }
}

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});

//make this component available to the app
export default ReportScreen;
