//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, ImageBackground, Dimensions, TouchableOpacity, ScrollView } from 'react-native';
import Pdf from 'react-native-pdf';
import { Navigation } from 'react-native-navigation';
import Colors from '../../utilities/Colors';
import BaseComponent from '../../components/BaseComponent';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
// create a component
class QualityScreen extends Component {
  static options(passProps) {
    return {
      topBar: {
        visible: true,
        title: {
          text: 'Quality',
          color: 'white',
        },
        background: {
          color: Colors.PRIMARY_COLOR,
        },
        backButton: {
          color: '#fff',
          visible: true
        },
        rightButtons: [
          {
            id: 'home',
            icon: require('../../assets/images/ic_home.png')
          }
        ],
      },
      layout: {
        orientation: ['portrait'] // An array of supported orientations
      },
    };
  }

  constructor(props){
    super(props);
    Navigation.events().bindComponent(this);
  }

  navigationButtonPressed({ buttonId }) {
    if(buttonId === 'home'){
      Navigation.setStackRoot(this.props.componentId, {
        component: {
          name: 'HomeScreen',
        }
      });
    }
  }

  goToHighRise(){
    Navigation.push(this.props.componentId, {
      component: {
        name: 'ListDataScreen',
        passProps: {
          name: 'High Rise',
          parentName: 'Quality',
          params: [
            {
              name: 'nama_sub_modul_i',
              data: '1.1. Konstruksi',
            },
            {
              name: 'des_sub_modul_i',
              data: 'Berikut adalah modul High Rise'
            },
            {
              name: 'img_sub_modul_i',
              data: 'http://202.59.167.149/uploads/1/2018-03/high_rise_menu.png'
            },
            {
              name: 'master_modul_id',
              data: 1
            },
            {
              name: 'sub_modul_i_id',
              data: 1
            },
          ]
        }
      }
    });
  }

  goToLanded(){
    Navigation.push(this.props.componentId, {
      component: {
        name: 'ListDataScreen',
        passProps: {
          name: 'Landed',
          parentName: 'Quality',
          params: [
            {
              name: 'nama_sub_modul_i',
              data: '1.2. Landed House',
            },
            {
              name: 'des_sub_modul_i',
              data: 'Berikut adalah modul lended house'
            },
            {
              name: 'img_sub_modul_i',
              data: 'http://202.59.167.149/uploads/1/2018-03/landed_menu.png'
            },
            {
              name: 'master_modul_id',
              data: 1
            },
            {
              name: 'sub_modul_i_id',
              data: 2
            },
          ]
        }
      }
    });
  }

  goToPropertyOne(){
    Navigation.push(this.props.componentId, {
      component: {
        name: 'ListDataScreen',
        passProps: {
          name: 'Property 1',
          parentName: 'Quality',
          params: [
            {
              name: 'nama_sub_modul_i',
              data: '1.3 Property I',
            },
            {
              name: 'des_sub_modul_i',
              data: 'Berikut adalah modul property I'
            },
            {
              name: 'img_sub_modul_i',
              data: 'http://202.59.167.149/uploads/1/2018-03/properti_satu_menu.png'
            },
            {
              name: 'master_modul_id',
              data: 1
            },
            {
              name: 'sub_modul_i_id',
              data: 3
            },
          ]
        }
      }
    });
  }

  goToPropertyTwo(){
    Navigation.push(this.props.componentId, {
      component: {
        name: 'ListDataScreen',
        passProps: {
          name: 'Property 2',
          parentName: 'Quality',
          params: [
            {
              name: 'nama_sub_modul_i',
              data: '1.4 Property II',
            },
            {
              name: 'des_sub_modul_i',
              data: 'Berikut adalah modul property II'
            },
            {
              name: 'img_sub_modul_i',
              data: 'http://202.59.167.149/uploads/1/2018-03/properti_dua_menu.png'
            },
            {
              name: 'master_modul_id',
              data: 1
            },
            {
              name: 'sub_modul_i_id',
              data: 4
            },
          ]
        }
      }
    });
  }

  render() {
    return (
      <BaseComponent>
        <ImageBackground style={styles.container} source={require('../../assets/images/bg_menu.png')}>
          <View
            style={{backgroundColor: '#bfbfbf', padding: 5, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start'}}
          >
            <Image source={require('../../assets/images/kualitas_nav.png')} style={{width: 20, height: 20, resizeMode: 'contain'}}/>
            <Text style={{fontWeight: 'bold', fontSize: 13, marginLeft: 15, flex: 1, marginTop: 2}}>Quality</Text>
          </View>
          <ScrollView style={{flex: 3, alignSelf: 'stretch', padding: 10}}>
            <View style={{flexDirection: 'row'}}>
              <TouchableOpacity
                onPress={() => this.goToHighRise()}
                style={{flex: 1, backgroundColor: '#57ace6', marginBottom: 5, marginRight: 5, alignItems: 'center', justifyContent: 'center', paddingTop: 25, paddingBottom: 25}}
              >
                <View style={{alignItems: 'center', justifyContent: 'center'}}>
                  <Image source={require('../../assets/images/high_rise_menu1.png')}/>
                  <Text style={{fontWeight: 'bold', color: '#fff'}}>HIGH RISE</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.goToLanded()}
                style={{flex: 1, backgroundColor: '#57ace6', marginBottom: 5, marginLeft: 5, alignItems: 'center', justifyContent: 'center', paddingTop: 25, paddingBottom: 25}}
              >
                <View style={{alignItems: 'center', justifyContent: 'center'}}>
                  <Image source={require('../../assets/images/landed_menu.png')}/>
                  <Text style={{fontWeight: 'bold', color: '#fff'}}>LANDED</Text>
                </View>
              </TouchableOpacity>
            </View>
            <View style={{flexDirection: 'row'}}>
              <TouchableOpacity
                onPress={() => this.goToPropertyOne()}
                style={{flex: 1, backgroundColor: '#57ace6', marginTop: 5, marginRight: 5, alignItems: 'center', justifyContent: 'center', paddingTop: 25, paddingBottom: 25}}
              >
                <View style={{alignItems: 'center', justifyContent: 'center'}}>
                  <Image source={require('../../assets/images/properti_satu_menu.png')}/>
                  <Text style={{fontWeight: 'bold', color: '#fff'}}>PROPERTY 1</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.goToPropertyTwo()}
                style={{flex: 1, backgroundColor: '#57ace6', marginTop: 5, marginLeft: 5, alignItems: 'center', justifyContent: 'center', paddingTop: 25, paddingBottom: 25}}
              >
                <View style={{alignItems: 'center', justifyContent: 'center'}}>
                  <Image source={require('../../assets/images/properti_dua_menu.png')}/>
                  <Text style={{fontWeight: 'bold', color: '#fff'}}>PROPERTY 2</Text>
                </View>
              </TouchableOpacity>
            </View>
          </ScrollView>
        </ImageBackground>
      </BaseComponent>
    );
  }
}

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});

//make this component available to the app
export default QualityScreen;
