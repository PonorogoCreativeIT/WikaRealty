//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, NetInfo, AsyncStorage } from 'react-native';
import firebase from 'react-native-firebase';

// create a component
class BaseComponent extends Component { 

  constructor(props){
    super(props);

    this.state = {
      status: true,
    }
  }
  componentDidMount() {
    NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectionChange);

    NetInfo.isConnected.fetch().done(
      (isConnected) => { this.setState({ status: isConnected }); }
    );

    this.createNotificationListeners();
    this.checkPermissionNotification();
  }

  componentWillUnmount() {
    NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectionChange);
  }

  handleConnectionChange = (isConnected) => {
    this.setState({ status: isConnected });
  }

  async checkPermissionNotification(){
    try {
      await firebase.messaging().requestPermission();
    } catch (error) {
    }
  }

  async createNotificationListeners() {
    try{
      this.messageListener = firebase.messaging().onMessage((data) => {
        this.setData(data);
      });
    }catch(error){

    }
  }

  async setData(data){
    try{
      const tempNotifications = await AsyncStorage.getItem('notifications');
      let notifications = JSON.parse(tempNotifications).reverse();
      notifications.push(data);
      notifications.reverse();
      // alert(JSON.stringify(notifications));
      await AsyncStorage.setItem('notifications', JSON.stringify(notifications));
    }catch(error){

    }
  }

  render() {
    return (
      <View style={styles.container}>
        {this.state.status === true &&
          <View style={{flex: 1, alignSelf: 'stretch'}}>
            {this.props.children}
          </View>
        }
        {this.state.status === false &&
          <Image source={require('../assets/images/no_found.png')} style={{alignSelf: 'stretch', flex: 1}}/>
        }
      </View>
    );
  }
}

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

//make this component available to the app
export default BaseComponent;
