import Request from './utilities/Request';

class WikaAPI {
  static login(data) {
    return Request('login_mobile', {
      method: 'POST',
      data,
    })
  }

  static banner_slider() {
    return Request('banner_slider', {
      method: 'POST',
    })
  }

  static running_text() {
    return Request('running_text', {
      method: 'POST',
    })
  }

  static get_unit_kerja(data) {
    return Request('get_unit_kerja', {
      method: 'POST',
      data,
    })
  }

  static edit_user(data) {
    return Request('edit_user', {
      method: 'POST',
      data,
    })
  }

  static search_file_pdf() {
    return Request('search_file_pdf', {
      method: 'POST',
    })
  }

  static get_sub_modul_i(data) {
    return Request('get_sub_modul_i', {
      method: 'POST',
      data,
    })
  }

  static get_sub_modul_ii(data) {
    return Request('get_sub_modul_ii', {
      method: 'POST',
      data,
    })
  }

  static get_sub_modul_iii(data) {
    return Request('get_sub_modul_iii', {
      method: 'POST',
      data,
    })
  }

  static get_sub_modul_iv(data) {
    return Request('get_sub_modul_iv', {
      method: 'POST',
      data,
    })
  }

  static get_sub_modul_v(data) {
    return Request('get_sub_modul_v', {
      method: 'POST',
      data,
    })
  }

  static get_file_pdf_qshe_i(data) {
    return Request('get_file_pdf_qshe_i', {
      method: 'POST',
      data,
    })
  }

  static get_file_pdf_qshe_ii(data) {
    return Request('get_file_pdf_qshe_ii', {
      method: 'POST',
      data,
    })
  }

  static get_file_pdf_qshe_iii(data) {
    return Request('get_file_pdf_qshe_iii', {
      method: 'POST',
      data,
    })
  }

  static get_file_pdf_qshe_iv(data) {
    return Request('get_file_pdf_qshe_iv', {
      method: 'POST',
      data,
    })
  }

  static get_file_pdf_qshe_v(data) {
    return Request('get_file_pdf_qshe_v', {
      method: 'POST',
      data,
    })
  }

  static log_user_mobile(data) {
    return Request('log_user_mobile', {
      method: 'POST',
      data,
    })
  }
}

export default WikaAPI;