import React, { Component } from 'react';
import { Alert, AsyncStorage } from 'react-native';

class Helpers extends Component {
  static message(title = '', error = '') {
    setTimeout(() => {
      Alert.alert(title, error);
    }, 100);
  }
}

export default Helpers;
