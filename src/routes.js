import { Navigation } from 'react-native-navigation';
import LoginScreen from './screens/credential/LoginScreen';
import CheckCredentialScreen from './screens/credential/CheckCredentialScreen';
import HomeScreen from './screens/home/HomeScreen';
import SearchScreen from './screens/home/SearchScreen';
import NotificationScreen from './screens/home/NotificationScreen';
import ProfileScreen from './screens/home/ProfileScreen';
import PDFScreen from './screens/pdf/PDFScreen';
import QualityScreen from './screens/quality/QualityScreen';
import SafetyScreen from './screens/safety/SafetyScreen';
import ReportScreen from './screens/report/ReportScreen';
import WRInfoScreen from './screens/wrinfo/WRInfoScreen';
import ListDataScreen from './screens/data/ListDataScreen';

export function routes() {
    Navigation.registerComponent('LoginScreen', () => LoginScreen);
    Navigation.registerComponent('CheckCredentialScreen', () => CheckCredentialScreen);
    Navigation.registerComponent('HomeScreen', () => HomeScreen);
    Navigation.registerComponent('SearchScreen', () => SearchScreen);
    Navigation.registerComponent('NotificationScreen', () => NotificationScreen);
    Navigation.registerComponent('ProfileScreen', () => ProfileScreen);
    Navigation.registerComponent('PDFScreen', () => PDFScreen);
    Navigation.registerComponent('QualityScreen', () => QualityScreen);
    Navigation.registerComponent('SafetyScreen', () => SafetyScreen);
    Navigation.registerComponent('ReportScreen', () => ReportScreen);
    Navigation.registerComponent('WRInfoScreen', () => WRInfoScreen);
    Navigation.registerComponent('ListDataScreen', () => ListDataScreen);
}